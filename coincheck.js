$status = {
  btc: "xxxx",
  xrp: "xx.xxxx",
  xem: "xx.xxxx",
  str: ".xxxxxxx"
};

function floatFormat(number, n){
  var _pow = Math.pow(10,n);
  return Math.round(number * _pow) / _pow;
}

function updateSubtitle() {
  var fmt = "BTC:$btc\nXRP:$xrp\nXEM:$xem\nSTR:$str";
  simply.subtitle(util2.format(fmt, $status));
}

function updateBody() {
  simply.body("");
}

function zeroFill(num, fill) {
  var padd = "0000000000";
  return (padd + num).slice(-fill);
}

function timeText() {
  var now = new Date();
  var n = {
    month: zeroFill(now.getMonth()+1,2),
    date: zeroFill(now.getDate(),2),
    hours: zeroFill(now.getHours(),2),
    minutes: zeroFill(now.getMinutes(),2)
  }
  return util2.format("$month/$date - $hours:$minutes",n);
}

$tasks = {
  btc: function() {
    ajax({ url: 'https://coincheck.com/api/rate/btc_jpy', type:'json'}, function(data) {
      $status.btc = Math.floor(data["rate"]);
      updateSubtitle();
    });
  },
  xrp: function() {
    ajax({ url: 'https://coincheck.com/api/rate/xrp_jpy', type:'json'}, function(data) {
      $status.xrp = floatFormat(data["rate"],4);
      updateSubtitle();
    });
  },
  xem: function() {
    ajax({ url: 'https://coincheck.com/api/rate/xem_jpy', type:'json'}, function(data) {
      $status.xem = floatFormat(data["rate"],4);
      updateSubtitle();
    });
  },
  str: function() {
    ajax({url: 'https://poloniex.com/public?command=returnTicker', type:'json'}, function(data) {
      $status.str = floatFormat(Math.pow(10,6)*data["BTC_STR"].last,2) + 'mu';
      updateSubtitle();
    });
  }
}

function refresh() {
  simply.title(timeText());
  for (var task in $tasks) {
    $tasks[task]();
  }
  updateSubtitle();
  updateBody();
}

simply.on('singleClick', function(e){
  simply.vibe();
  refresh();
});

simply.begin = function() {
  try {
    console.log('Simply.js start');
    simply.fullscreen(true);
    refresh();
  } catch(e) {
    console.log(e);
    simply.body(e);
  }
};
